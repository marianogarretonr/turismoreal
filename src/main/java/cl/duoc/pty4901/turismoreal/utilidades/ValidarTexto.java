/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.utilidades;

import cl.duoc.pty4901.turismoreal.persistencia.Conexion;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JTable;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;


/**
 *
 * @author rmundaca
 */
public class ValidarTexto {

    //Metodo para revisar estado de un texto, retorna verdaddero o falso
        //verdadero para campos no vacios
        //falso para para campos vacios
    public boolean validarTextoVacio(JTextField objJTextField) {

        boolean estado = !objJTextField.getText().isBlank();

        if (!estado) {
            objJTextField.setBackground(Color.red);
        } else {
            objJTextField.setBackground(Color.white);
        }

        return estado;

    }

    //Metodo para revisar estado de un texto, retorna verdaddero o falso
        //verdadero para campos no vacios
        //falso para para campos vacios
    public boolean validarTextoVacio(JTextArea objJTextArea) {

        boolean estado = !objJTextArea.getText().isBlank();

        if (!estado) {
            objJTextArea.setBackground(Color.red);
        } else {
            objJTextArea.setBackground(Color.white);
        }

        return estado;
    }

    //Metodo para revisar estado de un texto, retorna verdaddero o falso
        //verdadero para campos no vacios
        //falso para para campos vacios    
    public boolean validarTextoVacio(JPasswordField objJPasswordField) {

        boolean estado = !objJPasswordField.getText().isBlank();

        if (!estado) {
            objJPasswordField.setBackground(Color.red);
        } else {
            objJPasswordField.setBackground(Color.white);
        }

        return estado;
    }
    
    //Metodo para revisa todos los JField, JArea y JPasswordField de un JPanel 
    //encontrado la existencia o ausencia de texto en ellos
        //Retorna true en caso de tener texto en todos los campos
        //Retorna false en caso de que algún campo no tenga texto, cambia de color a rojo
    public boolean ValidarCamposNoVacios(JPanel objJPanel) {

        boolean estado = true;

        Component[] componente = objJPanel.getComponents();

        for (Component componente1 : componente) {
            if (componente1 instanceof JTextField) {
                JTextField objJTextField = (JTextField) componente1;
                estado = estado && validarTextoVacio(objJTextField);
            }
            if (componente1 instanceof JTextArea) {
                JTextArea objJTextArea = (JTextArea) componente1;
                estado = estado && validarTextoVacio(objJTextArea);
            }
            if (componente1 instanceof JPasswordField) {
                JPasswordField objJPasswordField = (JPasswordField) componente1;
                estado = estado && validarTextoVacio(objJPasswordField);
            }
        }
        return estado;
    }

    //Metodo que recorre un JPanel y vacia todos los campos que se encuentren en el
    public void colocarTextoVacio(JPanel objJPanel) {

        Component[] componente = objJPanel.getComponents();

        for (Component componente1 : componente) {
            if (componente1 instanceof JTextField) {
                JTextField objJTextField = (JTextField) componente1;
                objJTextField.setText("");
            }
            if (componente1 instanceof JTextArea) {
                JTextArea objJTextArea = (JTextArea) componente1;
                objJTextArea.setText("");
            }
        }
    }

    //Metodo que cambia de estado enable a disable o viceversa los campos de un JPanel
    public void cambiarEstadoEdicionCampos(JPanel objJPanel, boolean estado) {

        Component[] componente = objJPanel.getComponents();

        for (Component componente1 : componente) {
            if (componente1 instanceof JTextField) {
                JTextField objJTextField = (JTextField) componente1;
                objJTextField.setEditable(estado);
            }
            if (componente1 instanceof JTextArea) {
                JTextArea objJTextArea = (JTextArea) componente1;
                objJTextArea.setEditable(estado);
            }
            if (componente1 instanceof JComboBox) {
                JComboBox objJComboBox = (JComboBox) componente1;
                objJComboBox.enable(estado);
            }
        }
    }

    //Metodo que oculta la ultima columna de una JTable
    public void ocultarUltimaColumna(JTable objJTable) {

        int cantidad = objJTable.getColumnModel().getColumnCount() - 1;
        objJTable.getColumnModel().removeColumn(objJTable.getColumnModel().getColumn(cantidad));

    }

    //Metodo para cargar datos de un JComboBox
    public void cargarDatosJComboBox(Object objeto, List lista, JComboBox objJComboBox) {

        objJComboBox.removeAllItems();
        objJComboBox.addItem(objeto);
        lista.forEach(dato -> {
            objJComboBox.addItem(dato);
        });

    }

    //Metodo para verificar que solo se ingresen letras a un campo
    public boolean soloLetras(KeyEvent evt, Component comp) {

        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            evt.consume();
            JOptionPane.showMessageDialog(comp, "Igrese solo numeros");
        }
        return true;
    }

    //Metodo que valida cantidad de caracteres de un campo, requiere el campo, el largo maximo, el minimo y un alias para el campo
    public boolean validarCantidadCaracteres(JTextArea objJTextArea, int maxLargo, int minLargo, String nombre) {

        String Caracteres = objJTextArea.getText();

        if (Caracteres.length() >= minLargo && Caracteres.length() <= maxLargo) {

            return true;

        }

        JOptionPane.showMessageDialog(null, nombre + " Debe tener entre " 
                + minLargo + " y " + maxLargo + " de caracteres");

        return false;
    }

    //Metodo que valida cantidad de caracteres de un campo, requiere el campo, el largo maximo, el minimo y un alias para el campo
    public boolean validarCantidadCaracteres(JTextField objJTextField, int maxLargo, int minLargo, String nombre) {

        String Caracteres = objJTextField.getText();

        if (Caracteres.length() >= minLargo && Caracteres.length() <= maxLargo) {

            return true;

        }

        JOptionPane.showMessageDialog(null, nombre + " Debe tener entre " 
                + minLargo + " y " + maxLargo + " de caracteres");

        return false;

    }

    //Metodo que valida cantidad de caracteres de un campo, requiere el campo, el largo maximo, el minimo y un alias para el campo
    public boolean validarCantidadCaracteres(JPasswordField objJPasswordField, int maxLargo, int minLargo, String nombre) {

        String Caracteres = objJPasswordField.getText();

        if (Caracteres.length() >= minLargo && Caracteres.length() <= maxLargo) {

            return true;

        }

        JOptionPane.showMessageDialog(null, nombre + " Debe tener entre " 
                + minLargo + " y " + maxLargo + " de caracteres");

        return false;
    }
    
    //Meotodo para validar la conexion  a la BD 
    public boolean validarConexion(){
        
        Conexion objConexion = new Conexion();
               
        return objConexion.getConexion() == null;
    }
    
    //Meotod para validar el ingreso de solo numeros a un campo
    public void validarSoloNumeros(java.awt.event.KeyEvent evt, java.awt.Component component){
        char validar = evt.getKeyChar();
        
        if(Character.isLetter(validar)){
            component.getToolkit().beep();
            evt.consume();
            
            JOptionPane.showMessageDialog(null, "Solo debe ingresar Números");
        }
    }
    
    //Meotod para validar el ingreso de solo letras a un campo
    public void validarSoloLetras(java.awt.event.KeyEvent evt, java.awt.Component component){
        char validar = evt.getKeyChar();
        
        if(Character.isDigit(validar)){
            component.getToolkit().beep();
            evt.consume();
            
            JOptionPane.showMessageDialog(null, "Solo debe ingresar Letras");
        }
    }
    

}
