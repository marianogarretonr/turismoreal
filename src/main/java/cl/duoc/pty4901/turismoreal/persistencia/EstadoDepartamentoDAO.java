/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.EstadoDepartamento;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Doris
 */
public class EstadoDepartamentoDAO {

    public EstadoDepartamentoDAO() {
    }
    
    public List<EstadoDepartamento> mostrarEstadoDeartamento() {
        EstadoDepartamento objEstadoDepartamento=null;
        List<EstadoDepartamento> listadoEstadoDepartamento= new ArrayList<>();
        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_ESTADODEPARTAMENTO(?,?,?)");
            
            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);
            
            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {
                objEstadoDepartamento= new EstadoDepartamento();
                objEstadoDepartamento.setIdEstado(resultado.getInt(1));
               objEstadoDepartamento.setGlosa(resultado.getString(2));
               
               listadoEstadoDepartamento.add(objEstadoDepartamento);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoEstadoDepartamento;
    }
    
}
