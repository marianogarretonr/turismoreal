/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author santa
 */
public class Conexion {
    
    //Variables con datos basicos de la conexion
    String ipConexion = "localhost";//190.163.249.74";
    String usuario = "turismoReal";
    String contrasenia = "80868569";
    
    //Metodo que entrega el estado de la conexion, en caso de entregar null, no existe una conexión en el momento
    public Connection getConexion() {
        
        Connection con;
        
        try{        
            con = DriverManager.getConnection("jdbc:oracle:thin:@" + ipConexion
                + ":5000:orcl", usuario, contrasenia);
        }catch(SQLException e){
            con = null;
            System.out.println("Error al conectar la Base de Datos" + e.getMessage());
        }        
        
        return con;
    }
    
}
