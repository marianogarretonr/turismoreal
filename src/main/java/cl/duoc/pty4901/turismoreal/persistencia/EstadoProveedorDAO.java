/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.EstadoProveedor;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Doris
 */
public class EstadoProveedorDAO {

    public EstadoProveedorDAO() {
    }

    public List<EstadoProveedor> mostrarEstadoProveedor() {
        EstadoProveedor objEstadoProveedor = null;
        List<EstadoProveedor> listadoEstadoProveedor = new ArrayList<>();

        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_ESTADOPROVEEDOR (?,?,?)");

            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);

            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {
                objEstadoProveedor = new EstadoProveedor();
                objEstadoProveedor.setId_estado_proveedor(resultado.getInt(1));
                objEstadoProveedor.setDescripcion(resultado.getString(2));

                listadoEstadoProveedor.add(objEstadoProveedor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoEstadoProveedor;
    }

}
