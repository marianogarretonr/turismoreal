/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.Multa;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author santa
 */
public class MultaDAO {

    public MultaDAO() {
        
    }
    
    
     public List<Multa> mostrarMulta() {
         Multa objMulta = null;
         List<Multa> listadoMulta = new ArrayList<>();
      
         try{
             Conexion objConexion = new Conexion();
             CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_MULTA (?,?,?)");
            
             objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
             objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
             objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);
              
             objCallableStatement.execute();  
             ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
             while (resultado.next()) {  
                objMulta = new Multa();
                objMulta.setValor(resultado.getInt(1));
                objMulta.setDescripcion(resultado.getString(2));
                objMulta.setIdMulta(resultado.getInt(3));
             
                listadoMulta.add(objMulta);
            }                   
             }      
         catch(Exception e){
             e.printStackTrace();
         }      
        return listadoMulta;
    }

}