/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.Ciudad;
import cl.duoc.pty4901.turismoreal.entidad.Departamento;
import cl.duoc.pty4901.turismoreal.entidad.EstadoDepartamento;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Doris
 */
public class DepartamentoDAO {

    public DepartamentoDAO() {
    }

    public boolean agregarDepartamento(Departamento objDepartamento) {

        try {

            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_DEPARTAMENTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setString(1, objDepartamento.getNombreDepartamento());
            objCallableStatement.setInt(2, objDepartamento.getObjCiudad().getCodCiudad());
            objCallableStatement.setString(3, objDepartamento.getCalle());
            objCallableStatement.setInt(4, objDepartamento.getNroCalle());
            objCallableStatement.setString(5, objDepartamento.getPiso());
            objCallableStatement.setString(6, objDepartamento.getNroDepartamento());
            objCallableStatement.setInt(7, objDepartamento.getCantHabitaciones());
            objCallableStatement.setInt(8, objDepartamento.getCantBanios());
            objCallableStatement.setInt(9, objDepartamento.getCantPersonas());
            objCallableStatement.setString(10, objDepartamento.isAceptaMascotas());
            objCallableStatement.setString(11, objDepartamento.getDescripcion());
            objCallableStatement.setInt(12, objDepartamento.getValorDia());
            objCallableStatement.setInt(13, objDepartamento.getObjEstadoDepartamento().getIdEstado());
            objCallableStatement.registerOutParameter(14, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(15, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(16, OracleTypes.INTEGER);
            
            objCallableStatement.execute();

            objDepartamento.setId_dpto((int) objCallableStatement.getObject(16));

            DepartamentoDAO objDepartamentoDAO = new DepartamentoDAO();

            return true;

        } catch (Exception e) {
            System.out.println("Error al ingresar Departamento " + e.getMessage());
        }

        return false;
    }

    public Map<Integer, Departamento> mostrarDepartamento() {
        Departamento objDepartamento = null;
        Ciudad objCiudad = null;
        EstadoDepartamento objEstadoDepartamento = null;

        Map<Integer, Departamento> listadoDepartamento = new HashMap<>();
        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_EDITAR_DEPARTAMENTO(?,?,?)");

            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);

            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {

                objDepartamento = new Departamento();
                objEstadoDepartamento = new EstadoDepartamento();
                objCiudad = new Ciudad();

                objDepartamento.setId_dpto(resultado.getInt(1));
                objDepartamento.setNombreDepartamento(resultado.getString(2));
                objCiudad.setCodCiudad(resultado.getInt(3));  
                objCiudad.setNombreCiudad(resultado.getString(4));
                objDepartamento.setCalle(resultado.getString(5));
                objDepartamento.setNroCalle(resultado.getInt(6));
                objDepartamento.setPiso(resultado.getString(7));
                objDepartamento.setNroDepartamento(resultado.getString(8));
                objDepartamento.setCantHabitaciones(resultado.getInt(9));
                objDepartamento.setCantBanios(resultado.getInt(10));
                objDepartamento.setCantPersonas(resultado.getInt(11));
                objDepartamento.setAceptaMascotas(resultado.getString(12));
                objDepartamento.setDescripcion(resultado.getString(13));
                objDepartamento.setValorDia(resultado.getInt(14));
                
                objEstadoDepartamento.setIdEstado(resultado.getInt(15));
                objEstadoDepartamento.setGlosa(resultado.getString(16));
                
                objDepartamento.setObjCiudad(objCiudad);
                objDepartamento.setObjEstadoDepartamento(objEstadoDepartamento);
                
                listadoDepartamento.put(objDepartamento.getId_dpto(), objDepartamento);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoDepartamento;
    }
    
    public int actualizarDepartamento(Departamento objDepartamento){
         try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ACTUALIZAR_DEPARTAMENTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            
            objCallableStatement.setString(1, objDepartamento.getNombreDepartamento());
            objCallableStatement.setInt(2, objDepartamento.getObjCiudad().getCodCiudad());
            objCallableStatement.setString(3, objDepartamento.getCalle());
            objCallableStatement.setInt(4, objDepartamento.getNroCalle());
            objCallableStatement.setString(5, objDepartamento.getPiso());
            objCallableStatement.setString(6, objDepartamento.getNroDepartamento());
            objCallableStatement.setInt(7, objDepartamento.getCantHabitaciones());
            objCallableStatement.setInt(8, objDepartamento.getCantBanios());
            objCallableStatement.setInt(9, objDepartamento.getCantPersonas());
            objCallableStatement.setString(10, objDepartamento.isAceptaMascotas());
            objCallableStatement.setString(11, objDepartamento.getDescripcion());
            objCallableStatement.setInt(12, objDepartamento.getValorDia());
            objCallableStatement.setInt(13, objDepartamento.getObjEstadoDepartamento().getIdEstado());
            objCallableStatement.registerOutParameter(14, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(15, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(16, OracleTypes.INTEGER);
            
            objCallableStatement.execute();

            return objCallableStatement.getInt(16);

        } catch (Exception e) {
            System.out.println("Error al actualizar Departamento " + e.getMessage());
        }

        return -1;
    }

}
