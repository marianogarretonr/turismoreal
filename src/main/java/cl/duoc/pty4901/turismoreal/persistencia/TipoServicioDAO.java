/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.TipoServicio;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Doris
 */
public class TipoServicioDAO {

    public TipoServicioDAO() {
    }

    public List<TipoServicio> mostrarTipoServicio() {
        TipoServicio objTipoServicio = null;
        List<TipoServicio> listadoTipoServicio = new ArrayList<>();

        try {

            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_TIPOSERVICIO (?,?,?)");

            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);

            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {
                objTipoServicio = new TipoServicio();
                objTipoServicio.setId_tipo_servicio(resultado.getInt(1));
                objTipoServicio.setDescripcion(resultado.getString(2));

                listadoTipoServicio.add(objTipoServicio);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listadoTipoServicio;
    }

}
