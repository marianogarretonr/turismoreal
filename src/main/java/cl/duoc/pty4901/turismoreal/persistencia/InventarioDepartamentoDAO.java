/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.InventarioDepartamento;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Doris
 */
public class InventarioDepartamentoDAO {

    public InventarioDepartamentoDAO() {
    }

    public boolean agregarInventarioDepartamento(InventarioDepartamento objInventarioDepartamento) {
        
        try {
            
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_INVENTARIO_DEPARTAMENTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setString(1, objInventarioDepartamento.getEstadoLuces());
            objCallableStatement.setString(2, objInventarioDepartamento.getMicroondas());
            objCallableStatement.setString(3, objInventarioDepartamento.getHervidor());
            objCallableStatement.setString(4, objInventarioDepartamento.getJuguera());
            objCallableStatement.setString(5, objInventarioDepartamento.getWifi());
            objCallableStatement.setString(6, objInventarioDepartamento.getTvCable());
            objCallableStatement.setInt(7, objInventarioDepartamento.getCantFrazadas());
            objCallableStatement.setInt(8, objInventarioDepartamento.getCantVasos());
            objCallableStatement.setInt(9, objInventarioDepartamento.getCantTazas());
            objCallableStatement.setInt(10, objInventarioDepartamento.getCantPlatos());
            objCallableStatement.setInt(11, objInventarioDepartamento.getCantCubiertos());
            objCallableStatement.setInt(12, objInventarioDepartamento.getIdDepto());
            objCallableStatement.registerOutParameter(13, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(14, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(15, OracleTypes.INTEGER);

            objCallableStatement.execute();

            objInventarioDepartamento.setIdInventario((int) objCallableStatement.getObject(15));

            InventarioDepartamentoDAO objInventarioDepartamentoDAO = new InventarioDepartamentoDAO();

            return true;

        } catch (Exception e) {
            System.out.println("Error al ingresar Inventario Departamento debido a  " + e.getMessage());
        }

        return false;

    }

    public Map<Integer, InventarioDepartamento> mostrarInventarioDepartamento() {
        InventarioDepartamento objInventarioDepartamento = null;

        Map<Integer, InventarioDepartamento> listadoInventario = new HashMap<>();

        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_EDITAR_INVENTARIO_DEPARTAMENTO(?,?,?)");

            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);

            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {

                objInventarioDepartamento = new InventarioDepartamento();

                objInventarioDepartamento.setEstadoLuces(resultado.getString(1));
                objInventarioDepartamento.setMicroondas(resultado.getString(2));
                objInventarioDepartamento.setHervidor(resultado.getString(3));
                objInventarioDepartamento.setJuguera(resultado.getString(4));
                objInventarioDepartamento.setWifi(resultado.getString(5));
                objInventarioDepartamento.setTvCable(resultado.getString(6));
                objInventarioDepartamento.setCantFrazadas(resultado.getInt(7));
                objInventarioDepartamento.setCantVasos(resultado.getInt(8));
                objInventarioDepartamento.setCantTazas(resultado.getInt(9));
                objInventarioDepartamento.setCantPlatos(resultado.getInt(10));
                objInventarioDepartamento.setCantCubiertos(resultado.getInt(11));
                objInventarioDepartamento.setIdDepto(resultado.getInt(12));
                objInventarioDepartamento.setIdInventario(resultado.getInt(13));

                listadoInventario.put(objInventarioDepartamento.getIdInventario(), objInventarioDepartamento);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listadoInventario;
    }

    public int actualizarInventarioDepartamento(InventarioDepartamento objInventarioDepartamento) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ACTUALIZAR_INVENTARIO_DEPARTAMENTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setString(1, objInventarioDepartamento.getEstadoLuces());
            objCallableStatement.setString(2, objInventarioDepartamento.getMicroondas());
            objCallableStatement.setString(3, objInventarioDepartamento.getHervidor());
            objCallableStatement.setString(4, objInventarioDepartamento.getJuguera());
            objCallableStatement.setString(5, objInventarioDepartamento.getWifi());
            objCallableStatement.setString(6, objInventarioDepartamento.getTvCable());
            objCallableStatement.setInt(7, objInventarioDepartamento.getCantFrazadas());
            objCallableStatement.setInt(8, objInventarioDepartamento.getCantVasos());
            objCallableStatement.setInt(9, objInventarioDepartamento.getCantTazas());
            objCallableStatement.setInt(10, objInventarioDepartamento.getCantPlatos());
            objCallableStatement.setInt(11, objInventarioDepartamento.getCantCubiertos());
            objCallableStatement.setInt(12, objInventarioDepartamento.getIdDepto());
            objCallableStatement.setInt(13, objInventarioDepartamento.getIdInventario());
            objCallableStatement.registerOutParameter(14, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(15, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(16, OracleTypes.INTEGER);
            objCallableStatement.execute();

            return objCallableStatement.getInt(16);

        } catch (Exception e) {
            System.out.println("Error al actualizar Cliente " + e.getMessage());
        }

        return -1;

    }

}
