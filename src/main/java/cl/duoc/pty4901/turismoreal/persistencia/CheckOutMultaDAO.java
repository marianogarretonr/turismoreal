/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.CheckOutMulta;
import java.sql.CallableStatement;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;

/**
 *
 * @author santa
 */
public class CheckOutMultaDAO {
    
    private static final Logger log = Logger.getLogger(CheckOutMultaDAO.class);
    
    public CheckOutMultaDAO() {
    }   


    public int agregarCheckOutMulta(CheckOutMulta objCheckOutMulta) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_CHECKOUTMULTA(?,?,?,?,?)");

            objCallableStatement.setInt(1, objCheckOutMulta.getObjMulta().getIdMulta());
            objCallableStatement.setInt(2, objCheckOutMulta.getIdCheckOut());
            objCallableStatement.registerOutParameter(3, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(4, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(5, OracleTypes.INTEGER);

            objCallableStatement.execute();

            objCheckOutMulta.setId((int) objCallableStatement.getObject(5));

            //CheckOutDAO objCheckOutDAO = new CheckOutDAO();
            log.info((int)objCallableStatement.getObject(5));
            return (int)objCallableStatement.getObject(5);

            //objUsuarioDAO.agregarClienteEmpresa(new ClienteEmpresa(objCliente, new Empresa(Main.objGlobalEmpresa.getIdEmpresa())));

        } catch (Exception e) {
            log.error("Error al ingresar CheckOut Multa " + e.getMessage());
        }
        return 0;
    }
   
}

    