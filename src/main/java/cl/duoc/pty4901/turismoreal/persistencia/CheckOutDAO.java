/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.CheckOUT;
import java.sql.CallableStatement;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author santa
 */
public class CheckOutDAO {
    
    public CheckOutDAO() {
    }   


    public int agregarCheckOut(CheckOUT objCheckOut) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_CHECKOUT(?,?,?,?,?,?,?)");

            objCallableStatement.setString(1, objCheckOut.getMulta());
            objCallableStatement.setString(2, objCheckOut.getObservacion());
            objCallableStatement.setString(3, objCheckOut.getFechaCheckOut());
            objCallableStatement.setInt(4, objCheckOut.getCodReserva());
            objCallableStatement.registerOutParameter(5, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(6, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(7, OracleTypes.INTEGER);

            objCallableStatement.execute();

            objCheckOut.setIdCheckOut((int) objCallableStatement.getObject(7));

            //CheckOutDAO objCheckOutDAO = new CheckOutDAO();
            System.out.println((int)objCallableStatement.getObject(7));
            return (int)objCallableStatement.getObject(7);

            //objUsuarioDAO.agregarClienteEmpresa(new ClienteEmpresa(objCliente, new Empresa(Main.objGlobalEmpresa.getIdEmpresa())));

        } catch (Exception e) {
            System.out.println("Error al ingresar CheckOut " + e.getMessage());
        }
        return 0;
    }
   
}

    