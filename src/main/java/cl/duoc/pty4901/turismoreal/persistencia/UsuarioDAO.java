/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.Usuario;
import cl.duoc.pty4901.turismoreal.entidad.EstadoUsuario;
import cl.duoc.pty4901.turismoreal.entidad.TipoUsuario;
import cl.duoc.pty4901.turismoreal.entidad.Ciudad;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author santa
 */
public class UsuarioDAO {
    
    public UsuarioDAO() {
            }  
    
    private static final Logger log = Logger.getLogger(UsuarioDAO.class);


    public boolean agregarUsuario(Usuario objUsuario) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_USUARIO(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setInt(1, objUsuario.getRutUsuario());
            objCallableStatement.setString(2, objUsuario.getDvUsuario());
            objCallableStatement.setString(3, objUsuario.getNombreUsuario());
            objCallableStatement.setString(4, objUsuario.getApPaterno());
            objCallableStatement.setString(5, objUsuario.getApMaterno());
            objCallableStatement.setString(6, objUsuario.getNroTelefono());
            objCallableStatement.setString(7, objUsuario.getEmail());
            objCallableStatement.setInt(8, objUsuario.getObjCiudad().getCodCiudad());
            objCallableStatement.setString(9, objUsuario.getFrecuente());
            objCallableStatement.setInt(10, objUsuario.getObjTipoUsuario().getIdTipoUsuario());
            objCallableStatement.setInt(11, objUsuario.getObjEstadoUsuario().getIdEstadoUsuario());
            objCallableStatement.registerOutParameter(12, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(13, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(14, OracleTypes.INTEGER);

            objCallableStatement.execute();

            objUsuario.setId((int) objCallableStatement.getObject(14));

            UsuarioDAO objUsuarioDAO = new UsuarioDAO();

            //objUsuarioDAO.agregarClienteEmpresa(new ClienteEmpresa(objCliente, new Empresa(Main.objGlobalEmpresa.getIdEmpresa())));

            return true;

        } catch (Exception e) {
            System.out.println("Error al ingresar Cliente " );
            log.info(e);
        }
        return false;
    }
    
        //Listar todos los clientes
    public Map<Integer, Usuario> mostrarUsuario() {
        Usuario objUsuario = null;
        EstadoUsuario objEstadoUsuario = null;
        TipoUsuario objTipoUsuario = null;
        Ciudad objCiudad = null;

        Map<Integer, Usuario> listadoUsuario = new HashMap<>();
        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_EDITAR_USUARIO(?,?,?)");

            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);

            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {

                objUsuario = new Usuario();
                objEstadoUsuario = new EstadoUsuario();
                objTipoUsuario = new TipoUsuario();
                objCiudad = new Ciudad();

                objUsuario.setRutUsuario(resultado.getInt(1));
                objUsuario.setDvUsuario(resultado.getString(2));
                objUsuario.setNombreUsuario(resultado.getString(3));
                objUsuario.setApPaterno(resultado.getString(4));
                objUsuario.setApMaterno(resultado.getString(5));
                objUsuario.setNroTelefono(resultado.getString(6));
                objUsuario.setEmail(resultado.getString(7));

                objEstadoUsuario.setIdEstadoUsuario(resultado.getInt(8));
                objEstadoUsuario.setDescripcion(resultado.getString(9));
                
                objTipoUsuario.setIdTipoUsuario(resultado.getInt(10));
                objTipoUsuario.setDescripcion(resultado.getString(11));

                objCiudad.setCodCiudad(resultado.getInt(12));
                objCiudad.setNombreCiudad(resultado.getString(13));

                objUsuario.setObjCiudad(objCiudad);
                objUsuario.setObjEstadoUsuario(objEstadoUsuario);
                objUsuario.setObjTipoUsuario(objTipoUsuario);

                listadoUsuario.put(objUsuario.getRutUsuario(),objUsuario);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoUsuario;
    }
       
    public int actualizarUsuario(Usuario objUsuario) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ACTUALIZAR_USUARIO(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setInt(1, objUsuario.getRutUsuario());
            objCallableStatement.setString(2, objUsuario.getDvUsuario());
            objCallableStatement.setString(3, objUsuario.getNombreUsuario());
            objCallableStatement.setString(4, objUsuario.getApPaterno());
            objCallableStatement.setString(5, objUsuario.getApMaterno());
            objCallableStatement.setString(6, objUsuario.getNroTelefono());
            objCallableStatement.setString(7, objUsuario.getEmail());
            objCallableStatement.setInt(8, objUsuario.getObjCiudad().getCodCiudad());
            objCallableStatement.setString(9, objUsuario.getFrecuente());
            objCallableStatement.setInt(10, objUsuario.getObjTipoUsuario().getIdTipoUsuario());
            objCallableStatement.setInt(11, objUsuario.getObjEstadoUsuario().getIdEstadoUsuario());
            objCallableStatement.registerOutParameter(12, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(13, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(14, OracleTypes.INTEGER);

            objCallableStatement.execute();

            return objCallableStatement.getInt(14);

        } catch (Exception e) {
            System.out.println("Error al actualizar Cliente " + e.getMessage());
            log.error(e);
        }

        return -1;
    }
    
    public TipoUsuario ValidarUsuario(Usuario usuario) {
       
        TipoUsuario objTipoUsuario = new TipoUsuario();
        
        try {
            Conexion objConexion = new Conexion();
            
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_VALIDAR_USUARIO(?,?,?,?,?)");
            objCallableStatement.setString(1, usuario.getEmail());
            objCallableStatement.setString(2, usuario.getContrasenia());
            objCallableStatement.registerOutParameter(3, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(4, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(5, OracleTypes.CURSOR);
            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(5);
            while (resultado.next()) {
                
                objTipoUsuario.setIdTipoUsuario(resultado.getInt(1));
                objTipoUsuario.setDescripcion(resultado.getString(2));
                
            }        
            return objTipoUsuario;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

    