/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.CheckIN;
import cl.duoc.pty4901.turismoreal.entidad.Reserva;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import oracle.jdbc.OracleTypes;
import java.util.Map;

/**
 *
 * @author santa
 */
public class CheckINDAO {
    
    public CheckINDAO() {
    }   


    public boolean agregarCheckIn(CheckIN objCheckIn) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_CHECKIN(?,?,?,?,?,?,?,?)");

            objCallableStatement.setString(1, objCheckIn.getEntregaRegalo());
            objCallableStatement.setString(2, objCheckIn.getEstadoPago());
            objCallableStatement.setString(3, objCheckIn.getObservacion());
            objCallableStatement.setString(4, objCheckIn.getFechaCheckIn());
            objCallableStatement.setInt(5, objCheckIn.getCodReserva());
            objCallableStatement.registerOutParameter(6, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(7, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(8, OracleTypes.INTEGER);

            objCallableStatement.execute();

            objCheckIn.setIdCheckIn((int) objCallableStatement.getObject(8));

            CheckINDAO objCheckInDAO = new CheckINDAO();

            //objUsuarioDAO.agregarClienteEmpresa(new ClienteEmpresa(objCliente, new Empresa(Main.objGlobalEmpresa.getIdEmpresa())));

            return true;

        } catch (Exception e) {
            System.out.println("Error al ingresar Cliente " + e.getMessage());
        }
        return false;
    }
    
     /*public int listarReservaDpto(Reserva objReserva) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_LISTAR_RESERVADPTO(?,?,?)");

            objCallableStatement.setInt(1, objReserva.getCodReserva());
            objCallableStatement.registerOutParameter(12, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(13, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(14, OracleTypes.INTEGER);

            objCallableStatement.execute();

            return objCallableStatement.getInt(4);

        } catch (Exception e) {
            System.out.println("Error al listar Reserva " + e.getMessage());
        }

        return -1;
    }*/
}

    