/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.Departamento;
import cl.duoc.pty4901.turismoreal.entidad.Reserva;
import cl.duoc.pty4901.turismoreal.entidad.Usuario;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import oracle.jdbc.OracleTypes;
import java.util.Map;

/**
 *
 * @author santa
 */
public class ReservaDAO {
    
    public ReservaDAO() {
    }   


    public Map<Integer, Reserva>listarReservaDpto(int codReserva){
        Reserva objReserva = null;
        Departamento objDepartamento = null;

        Map<Integer, Reserva> listadoReserva = new HashMap<>();
        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_RESERVADPTO(?,?,?,?)");

            objCallableStatement.setInt(1, codReserva);
            objCallableStatement.registerOutParameter(2, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(3, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(4, OracleTypes.CURSOR);

            objCallableStatement.executeUpdate();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(4);
            while (resultado.next()) {

                objReserva = new Reserva();
                objDepartamento = new Departamento();

                objReserva.setCodReserva(codReserva);
                objReserva.getObjUsuario().setRutUsuario(resultado.getInt(1));
                objReserva.getObjUsuario().setNombreUsuario(resultado.getString(2));
                objReserva.getObjUsuario().setApPaterno((resultado.getString(3)));
                objReserva.getObjDepartamento().setNombreDepartamento(resultado.getString(4));
                objReserva.setMonto_reserva(resultado.getInt(5));
                objReserva.setMonto_pendiente(resultado.getInt(6));
                
                listadoReserva.put(objReserva.getCod_reserva(),objReserva);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoReserva;
    }
   
   /*public int listarReservaDpto(Reserva objReserva) {

        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ACTUALIZAR_USUARIO(?,?,?,?)");

            objCallableStatement.setString(1, objReserva.getCod_reserva());
            objCallableStatement.registerOutParameter(12, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(13, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(14, OracleTypes.INTEGER);

            objCallableStatement.execute();

            return objCallableStatement.getInt(4);

        } catch (Exception e) {
            System.out.println("Error al listar Reserva " + e.getMessage());
        }

        return -1;
    }*/
}

    