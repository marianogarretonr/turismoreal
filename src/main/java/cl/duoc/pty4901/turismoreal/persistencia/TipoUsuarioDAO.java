/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.TipoUsuario;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author santa
 */
public class TipoUsuarioDAO {

    public TipoUsuarioDAO() {
        
    }
    
    /**
     *
     * @param objTipoUsuario
     * @return
     */
    /*public boolean agregarComuna(Ciudad objCiudad){
                
        try{ 
            Conexion objConexion = new Conexion();
            
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_COMUNA(?,?,?,?,?)");            
            
            objCallableStatement.setString(1, objCiudad.getNombreCiudad());
            objCallableStatement.setInt(2, objCiudad.getObjPgetrovincia().getIdProvincia());
            objCallableStatement.registerOutParameter(3, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(4, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(5, OracleTypes.INTEGER);
            
            objCallableStatement.execute();
                        
            return true;
            
        }catch(Exception e){
            System.out.println("Error al ingresar Comuna " + e.getMessage());
        }                   
        return false;        
    }*/
    
    /**
     *
     * @param objComuna
     * @return
     */
    /*public boolean borrarComuna(Comuna objComuna){
        
        try {
            Conexion objConexion = new Conexion();
            
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ELIMINAR_COMUNA(?,?,?)");            
            objCallableStatement.setInt(1, objComuna.getIdComuna());
            objCallableStatement.registerOutParameter(2, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(3, OracleTypes.INTEGER);
            objCallableStatement.execute();
                        
            return true;
                                    
        } catch (Exception e) {
            System.out.println("Error al borrar Comuna " + e.getMessage());
        }
        
        return false;
    }*/
    
    /**
     *
     * @param objComuna
     * @return
     */
    /*public boolean actualizarComuna(Comuna objComuna){
        
        try {
            Conexion objConexion = new Conexion();
            
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ACTUALIZAR_COMUNA(?,?,?,?,?)");            
            
            objCallableStatement.setInt(1, objComuna.getIdComuna());
            objCallableStatement.setString(2, objComuna.getNombreComuna());
            objCallableStatement.setInt(3, objComuna.getObjProvincia().getIdProvincia());
            objCallableStatement.registerOutParameter(4, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(5, OracleTypes.INTEGER);
            
            objCallableStatement.execute();
                        
            return true;
                                    
        } catch (Exception e) {
            System.out.println("Error al actualizar Comuna " + e.getMessage());
        }
        
        return false;
    }*/
    
    /**
     *
     * @param idComuna
     * @return
     */
    /*public Ciudad listarCiudad(int codCiudad){
        
        Ciudad objComuna = new Ciudad();                
        
        try {
            Conexion objConexion = new Conexion();
            
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_LEER_COMUNA(?,?,?,?)");            
            
            objCallableStatement.setInt(1, codCiudad);
            objCallableStatement.registerOutParameter(2, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(3, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(4, OracleTypes.CURSOR);
            
            objCallableStatement.execute();
            
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(4);
                     
            while (resultado.next()) {     
                
            	objComuna.setIdComuna(resultado.getInt(1));
            	objComuna.setNombreComuna(resultado.getString(2));
                
                ProvinciaDAO objProvinciaDAO = new ProvinciaDAO();
                Provincia objProvincia = objProvinciaDAO.listarProvincia(resultado.getInt(3));
                
                objComuna.setObjProvincia(objProvincia);
            }            
                                                                                    
            return objComuna;
                                    
        } catch (Exception e) {
            System.out.println("Error al listar Comuna " + e.getMessage());
        }
        
        return null;
    }*/
    
     public List<TipoUsuario> mostrarTipoUsuario() {
         TipoUsuario objTipoUsuario = null;
         List<TipoUsuario> listadoTipoUsuario = new ArrayList<>();
      
         try{
             Conexion objConexion = new Conexion();
             CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_TIPO_USUARIO (?,?,?)");
            
             objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
             objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
             objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);
              
             objCallableStatement.execute();  
             ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
             while (resultado.next()) {  
                objTipoUsuario = new TipoUsuario();
                objTipoUsuario.setIdTipoUsuario(resultado.getInt(1));
                objTipoUsuario.setDescripcion(resultado.getString(2));
             
                listadoTipoUsuario.add(objTipoUsuario);
            }                   
             }      
         catch(Exception e){
             e.printStackTrace();
         }      
        return listadoTipoUsuario;
    }

}
