/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.persistencia;

import cl.duoc.pty4901.turismoreal.entidad.Ciudad;
import cl.duoc.pty4901.turismoreal.entidad.EstadoProveedor;
import cl.duoc.pty4901.turismoreal.entidad.EstadoUsuario;
import cl.duoc.pty4901.turismoreal.entidad.Proveedor;
import cl.duoc.pty4901.turismoreal.entidad.TipoServicio;
import cl.duoc.pty4901.turismoreal.entidad.TipoUsuario;
import cl.duoc.pty4901.turismoreal.entidad.Usuario;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Doris
 */
public class ProveedorDAO {

    public ProveedorDAO() {
    }

    public boolean agregarProveedor(Proveedor objProveedor) {

        try {

            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_CREAR_PROVEEDOR(?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setInt(1, objProveedor.getRut_proveedor());
            objCallableStatement.setString(2, objProveedor.getNombre_proveedor());
            objCallableStatement.setInt(3, objProveedor.getObjTipoServicio().getId_tipo_servicio());
            objCallableStatement.setInt(4, objProveedor.getObjEstadoProveedor().getId_estado_proveedor());
            objCallableStatement.setInt(5, objProveedor.getValor_servicio());
            objCallableStatement.setInt(6, objProveedor.getObjCiudad().getCodCiudad());
            objCallableStatement.setString(7, objProveedor.getObservacion());
            objCallableStatement.registerOutParameter(8, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(9, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(10, OracleTypes.INTEGER);

            objCallableStatement.execute();

            objProveedor.setId_proveedor((int) objCallableStatement.getObject(10));

            ProveedorDAO objProveedorDAO = new ProveedorDAO();

            return true;

        } catch (Exception e) {
            System.out.println("Error al ingresar Proveedor " + e.getMessage());
        }
        return false;
    }

    public Map<Integer, Proveedor> mostrarProveedor() {
        Proveedor objProveedor = null;
        EstadoProveedor objEstadoProveedor = null;
        TipoServicio objTipoServicio = null;
        Ciudad objCiudad = null;

        Map<Integer, Proveedor> listadoProveedor = new HashMap<>();

        try {
            Conexion objConexion = new Conexion();
            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("CALL SP_LISTAR_EDITAR_PROVEEDOR(?,?,?)");

            objCallableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(2, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(3, OracleTypes.CURSOR);

            objCallableStatement.execute();
            ResultSet resultado = (ResultSet) objCallableStatement.getObject(3);
            while (resultado.next()) {

                objProveedor = new Proveedor();
                objEstadoProveedor = new EstadoProveedor();
                objTipoServicio = new TipoServicio();
                objCiudad = new Ciudad();
                objProveedor.setId_proveedor(resultado.getInt(1));
                objProveedor.setRut_proveedor(resultado.getInt(2));
                objProveedor.setNombre_proveedor(resultado.getString(3));

                objTipoServicio.setId_tipo_servicio(resultado.getInt(4));
                objTipoServicio.setDescripcion(resultado.getString(5));

                objEstadoProveedor.setId_estado_proveedor(resultado.getInt(6));
                objEstadoProveedor.setDescripcion(resultado.getString(7));

                objProveedor.setValor_servicio(resultado.getInt(8));

                objCiudad.setCodCiudad(resultado.getInt(9));
                objCiudad.setNombreCiudad(resultado.getString(10));

                objProveedor.setObservacion(resultado.getString(11));

                objProveedor.setObjTipoServicio(objTipoServicio);
                objProveedor.setObjEstadoProveedor(objEstadoProveedor);
                objProveedor.setObjCiudad(objCiudad);

                listadoProveedor.put(objProveedor.getId_proveedor(), objProveedor);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoProveedor;
    }

    public int actualizarProveedor(Proveedor objProveedor) {
        try {
            Conexion objConexion = new Conexion();

            CallableStatement objCallableStatement = objConexion.getConexion().prepareCall("call SP_ACTUALIZAR_PROVEEDOR(?,?,?,?,?,?,?,?,?,?)");

            objCallableStatement.setInt(1, objProveedor.getRut_proveedor());
            objCallableStatement.setString(2, objProveedor.getNombre_proveedor());
            objCallableStatement.setInt(3, objProveedor.getObjTipoServicio().getId_tipo_servicio());
            objCallableStatement.setInt(4, objProveedor.getObjEstadoProveedor().getId_estado_proveedor());
            objCallableStatement.setInt(5, objProveedor.getValor_servicio());
            objCallableStatement.setInt(6, objProveedor.getObjCiudad().getCodCiudad());
            objCallableStatement.setString(7, objProveedor.getObservacion());
            objCallableStatement.registerOutParameter(8, OracleTypes.VARCHAR);
            objCallableStatement.registerOutParameter(9, OracleTypes.INTEGER);
            objCallableStatement.registerOutParameter(10, OracleTypes.CURSOR);
            objCallableStatement.execute();

            return objCallableStatement.getInt(15);

        } catch (Exception e) {
            System.out.println("Error al actualizar Cliente " + e.getMessage());
        }

        return -1;
    }

}
