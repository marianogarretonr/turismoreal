/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.controlador;

import cl.duoc.pty4901.turismoreal.entidad.CheckIN;
import cl.duoc.pty4901.turismoreal.entidad.CheckOUT;
import cl.duoc.pty4901.turismoreal.entidad.CheckOutMulta;
import cl.duoc.pty4901.turismoreal.entidad.Ciudad;
import cl.duoc.pty4901.turismoreal.entidad.Departamento;
import cl.duoc.pty4901.turismoreal.entidad.EstadoDepartamento;
import cl.duoc.pty4901.turismoreal.entidad.EstadoProveedor;
import cl.duoc.pty4901.turismoreal.entidad.TipoUsuario;
import cl.duoc.pty4901.turismoreal.entidad.EstadoUsuario;
import cl.duoc.pty4901.turismoreal.entidad.InventarioDepartamento;
import cl.duoc.pty4901.turismoreal.entidad.Usuario;
import cl.duoc.pty4901.turismoreal.entidad.Multa;
import cl.duoc.pty4901.turismoreal.entidad.Proveedor;
import cl.duoc.pty4901.turismoreal.entidad.Reserva;
import cl.duoc.pty4901.turismoreal.entidad.TipoServicio;
import cl.duoc.pty4901.turismoreal.persistencia.CheckINDAO;
import cl.duoc.pty4901.turismoreal.persistencia.CheckOutDAO;
import cl.duoc.pty4901.turismoreal.persistencia.CheckOutMultaDAO;
import cl.duoc.pty4901.turismoreal.persistencia.CiudadDAO;
import cl.duoc.pty4901.turismoreal.persistencia.DepartamentoDAO;
import cl.duoc.pty4901.turismoreal.persistencia.EstadoDepartamentoDAO;
import cl.duoc.pty4901.turismoreal.persistencia.EstadoProveedorDAO;
import cl.duoc.pty4901.turismoreal.persistencia.TipoUsuarioDAO;
import cl.duoc.pty4901.turismoreal.persistencia.EstadoUsuarioDAO;
import cl.duoc.pty4901.turismoreal.persistencia.InventarioDepartamentoDAO;
import cl.duoc.pty4901.turismoreal.persistencia.UsuarioDAO;
import cl.duoc.pty4901.turismoreal.persistencia.MultaDAO;
import cl.duoc.pty4901.turismoreal.persistencia.ProveedorDAO;
import cl.duoc.pty4901.turismoreal.persistencia.ReservaDAO;
import cl.duoc.pty4901.turismoreal.persistencia.TipoServicioDAO;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Doris
 */
public class Controlador {
    
    private CiudadDAO objCiudadDAO;
    private TipoUsuarioDAO objTipoUsuarioDAO;
    private EstadoUsuarioDAO objEstadoUsuarioDAO;
    private UsuarioDAO objUsuarioDAO;
    private MultaDAO objMultaDAO;
    private EstadoProveedorDAO objEstadoProveedorDAO;
    private TipoServicioDAO objTipoServicioDAO;
    private ProveedorDAO OBJProveedorDAO;
    private EstadoDepartamentoDAO objEstadoDepartamentoDAO;
    private DepartamentoDAO ObjDepartamentoDAO;
    private InventarioDepartamentoDAO objInventarioDepartamentoDAO;
    private ReservaDAO objReservaDAO;
    private CheckINDAO objCheckIn;
    private CheckOutDAO objCheckOutDAO;
    private CheckOutMultaDAO objCheckOutMultaDAO;
    
    public Controlador() {
        this.objCiudadDAO = new CiudadDAO();
        this.objTipoUsuarioDAO = new TipoUsuarioDAO();
        this.objEstadoUsuarioDAO = new EstadoUsuarioDAO();
        this.objUsuarioDAO = new UsuarioDAO();
        this.objMultaDAO = new MultaDAO();
         this.objEstadoProveedorDAO = new EstadoProveedorDAO();
        this.objTipoServicioDAO = new TipoServicioDAO();
        this.OBJProveedorDAO = new ProveedorDAO();
        this.objEstadoDepartamentoDAO = new EstadoDepartamentoDAO();
        this.ObjDepartamentoDAO = new DepartamentoDAO();
        this.objInventarioDepartamentoDAO = new InventarioDepartamentoDAO();
        this.objReservaDAO = new ReservaDAO();
        this.objCheckIn = new CheckINDAO();
        this.objCheckOutDAO = new CheckOutDAO();
        this.objCheckOutMultaDAO = new CheckOutMultaDAO();
        /*this.objEmpresaDAO = new EmpresaDAO();
        this.objEstadoClienteDAO = new EstadoClienteDAO();
        this.objEstadoEmpresaDAO = new EstadoEmpresaDAO();
        this.objEstadoPedidoDAO = new EstadoPedidoDAO();
        this.objEstadoProveedorDAO = new EstadoProveedorDAO();
        this.objEstadoUsuarioDAO = new EstadoUsuarioDAO();
        this.objFamiliaProductoDAO = new FamiliaProductoDAO();
        this.objFiadoDAO = new FiadoDAO();
        this.objOrdenPedidoDAO = new OrdenPedidoDAO();
        this.objProductoDAO = new ProductoDAO();
        this.objProveedorDAO = new ProveedorDAO();
        this.objRegionDAO = new RegionDAO();
        this.objProvinciaDAO = new ProvinciaDAO();
        this.objPagoDAO = new TipoPagoDAO();
        this.objUsuarioDAO = new UsuarioDAO();
        this.objVentaDAO = new VentaDAO();
        this.objVistaDAO = new VistaDAO();
        this.objTipoProductoDAO = new TipoProductoDAO();
        this.objVistaUsuarioDAO = new VistaUsuarioDAO();*/

    }
    
    public List<Ciudad> mostrarCiudad() {
    return this.objCiudadDAO.mostrarCiudad();
    }
    
    public List<TipoUsuario> mostrarTipoUsuarios() {
    return this.objTipoUsuarioDAO.mostrarTipoUsuario();
    }
    
    public List<EstadoUsuario> mostrarEstadoUsuario() {
    return this.objEstadoUsuarioDAO.mostrarEstadoUsuario();
    }
    
    public boolean agregarUsuario(Usuario usuario) {
    return this.objUsuarioDAO.agregarUsuario(usuario);
    }
    
    public Map<Integer, Usuario> mostrarUsuario() {
    return this.objUsuarioDAO.mostrarUsuario();
    }
    
    public int actualizarUsuario(Usuario usuario) {
    return this.objUsuarioDAO.actualizarUsuario(usuario);
    } 
    
    public List<Multa> mostrarMulta(){
    return this.objMultaDAO.mostrarMulta();
    }
      
    public List<EstadoProveedor> mostrarEstadoProveedor() {
        return this.objEstadoProveedorDAO.mostrarEstadoProveedor();
    }

    public List<TipoServicio> mostrarTipoServicio() {
        return this.objTipoServicioDAO.mostrarTipoServicio();
    }

    public boolean agregarProveedor(Proveedor proveedor) {
        return this.OBJProveedorDAO.agregarProveedor(proveedor);
    }

    public List<EstadoDepartamento> mostrarEstadoDepartamento() {
        return this.objEstadoDepartamentoDAO.mostrarEstadoDeartamento();
    }

    public boolean agregarDepartamento(Departamento departamento) {
        return this.ObjDepartamentoDAO.agregarDepartamento(departamento);
    }

    public boolean agregarInventarioDepartamento(InventarioDepartamento inventario) {
        return this.objInventarioDepartamentoDAO.agregarInventarioDepartamento(inventario);
    }
    
    public Map<Integer, Proveedor> mostrarProveedor() {
        return this.OBJProveedorDAO.mostrarProveedor();
    } 
    
    public int actualizarProveedor(Proveedor proveedor) {
        return this.OBJProveedorDAO.actualizarProveedor(proveedor);
    } 
     
    public Map<Integer, Departamento> mostrarDepartamento() {
        return this.ObjDepartamentoDAO.mostrarDepartamento();
    } 
    
    public int actualizarDepartamento(Departamento departamento) {
        return this.ObjDepartamentoDAO.actualizarDepartamento(departamento);
    } 
     
    public Map<Integer, InventarioDepartamento> mostrarInventarioDepartamento() {
        return this.objInventarioDepartamentoDAO.mostrarInventarioDepartamento();
    } 
    
    public int actualizarInventarioDepartamento(InventarioDepartamento inventarioDepartamento) {
        return this.objInventarioDepartamentoDAO.actualizarInventarioDepartamento(inventarioDepartamento);
    } 

    public Map<Integer, Reserva> listarReservaDpto(int codReserva) {
        return this.objReservaDAO.listarReservaDpto(codReserva);
    }
    
    public boolean agregarCheckIn(CheckIN checkIn) {
        return this.objCheckIn.agregarCheckIn(checkIn);
    }
    
    public int agregarCheckOut(CheckOUT checkOut) {
        return this.objCheckOutDAO.agregarCheckOut(checkOut);
    }
    
    public TipoUsuario validarUsuario(Usuario usuario){
        return this.objUsuarioDAO.ValidarUsuario(usuario);
    }

    public int agregarCheckOutMulta(CheckOutMulta objCheckOutMulta){
        return this.objCheckOutMultaDAO.agregarCheckOutMulta(objCheckOutMulta);
    }
    
}
