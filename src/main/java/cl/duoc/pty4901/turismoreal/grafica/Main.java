/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.grafica;

import cl.duoc.pty4901.turismoreal.entidad.Usuario;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 *
 * @author Doris
 */
public class Main {
    
    private static final Logger log = Logger.getLogger(Main.class);

    //Metodos estaticos (accesible por todo el codigo) para crear las varibles globales
    public static Usuario objGlobalEmpresa = new Usuario();
    public static String usuarioGlobal;
    
    public static String user = "Usuario";
    public static String password = "123456789";
    
    public static void main(String[] args) {
        BasicConfigurator.configure();
        log.info(user);
        Inicio inicio = new Inicio();
        inicio.show();
    }
    
}
