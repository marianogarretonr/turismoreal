/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author maria
 */
public class ProveedorReserva {
    private Reserva objReserva;
    private Proveedor objProveedor;

    public ProveedorReserva() {
    }

    public ProveedorReserva(Reserva objReserva, Proveedor objProveedor) {
        this.objReserva = objReserva;
        this.objProveedor = objProveedor;
    }

    public Reserva getObjReserva() {
        return objReserva;
    }

    public void setObjReserva(Reserva objReserva) {
        this.objReserva = objReserva;
    }

    public Proveedor getObjProveedor() {
        return objProveedor;
    }

    public void setObjProveedor(Proveedor objProveedor) {
        this.objProveedor = objProveedor;
    }

    @Override
    public String toString() {
        return "ProveedorReserva{" + "objReserva=" + objReserva + ", objProveedor=" + objProveedor + '}';
    }
    
    
}
