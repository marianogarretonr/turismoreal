/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author maria
 */
public class Proveedor {
    private int rut_proveedor;
    private String nombre_proveedor;
    private TipoServicio objTipoServicio;
    private EstadoProveedor objEstadoProveedor;
    private int valor_servicio;
    private Ciudad objCiudad;
    private String observacion;
    private int id_proveedor;

    public Proveedor() {
    }

    public Proveedor(int rut_proveedor, String nombre_proveedor, TipoServicio objTipoServicio, EstadoProveedor objEstadoProveedor, int valor_servicio, Ciudad objCiudad, String observacion, int id_proveedor) {
        this.rut_proveedor = rut_proveedor;
        this.nombre_proveedor = nombre_proveedor;
        this.objTipoServicio = objTipoServicio;
        this.objEstadoProveedor = objEstadoProveedor;
        this.valor_servicio = valor_servicio;
        this.objCiudad = objCiudad;
        this.observacion = observacion;
        this.id_proveedor = id_proveedor;
    }

    
    
     public Proveedor(int rut_proveedor, String nombre_proveedor, TipoServicio objTipoServicio, EstadoProveedor objEstadoProveedor, int valor_servicio, Ciudad objCiudad, String observacion) {
        this.rut_proveedor = rut_proveedor;
        this.nombre_proveedor = nombre_proveedor;
        this.objTipoServicio = objTipoServicio;
        this.objEstadoProveedor = objEstadoProveedor;
        this.valor_servicio = valor_servicio;
        this.objCiudad = objCiudad;
        this.observacion = observacion;
    }

    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public int getRut_proveedor() {
        return rut_proveedor;
    }

    public void setRut_proveedor(int rut_proveedor) {
        this.rut_proveedor = rut_proveedor;
    }

    public String getNombre_proveedor() {
        return nombre_proveedor;
    }

    public void setNombre_proveedor(String nombre_proveedor) {
        this.nombre_proveedor = nombre_proveedor;
    }

    public TipoServicio getObjTipoServicio() {
        return objTipoServicio;
    }

    public void setObjTipoServicio(TipoServicio objTipoServicio) {
        this.objTipoServicio = objTipoServicio;
    }

    public EstadoProveedor getObjEstadoProveedor() {
        return objEstadoProveedor;
    }

    public void setObjEstadoProveedor(EstadoProveedor objEstadoProveedor) {
        this.objEstadoProveedor = objEstadoProveedor;
    }

    public int getValor_servicio() {
        return valor_servicio;
    }

    public void setValor_servicio(int valor_servicio) {
        this.valor_servicio = valor_servicio;
    }

    public Ciudad getObjCiudad() {
        return objCiudad;
    }

    public void setObjCiudad(Ciudad objCiudad) {
        this.objCiudad = objCiudad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public String toString() {
        return "Proveedor{" + "rut_proveedor=" + rut_proveedor + ", nombre_proveedor=" + nombre_proveedor + ", objTipoServicio=" + objTipoServicio + ", objEstadoProveedor=" + objEstadoProveedor + ", valor_servicio=" + valor_servicio + ", objCiudad=" + objCiudad + ", observacion=" + observacion + ", id_proveedor=" + id_proveedor + '}';
    }

  
    

    
}
