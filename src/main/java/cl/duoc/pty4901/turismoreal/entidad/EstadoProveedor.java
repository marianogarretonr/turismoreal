/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author maria
 */
public class EstadoProveedor {
    private int id_estado_proveedor;
    private String descripcion;

    public EstadoProveedor() {
    }

    public EstadoProveedor(int id_estado_proveedor, String descripcion) {
        this.id_estado_proveedor = id_estado_proveedor;
        this.descripcion = descripcion;
    }
    
    public EstadoProveedor(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId_estado_proveedor() {
        return id_estado_proveedor;
    }

    public void setId_estado_proveedor(int id_estado_proveedor) {
        this.id_estado_proveedor = id_estado_proveedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
