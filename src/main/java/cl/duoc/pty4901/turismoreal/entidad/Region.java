/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class Region {
    private int codRegion;
    private String glosa;

    /**
     *
     */
    public Region() {
    }

    /**
     *
     * @param codRegion
     * @param glosa
     */
    public Region(int codRegion, String glosa) {
        this.codRegion = codRegion;
        this.glosa = this.glosa;
    }

    /**
     *
     * @return
     */
    public int getCodRegion() {
        return codRegion;
    }

    /**
     *
     * @param codRegion
     */
    public void setCodRegion(int codRegion) {
        this.codRegion = codRegion;
    }

    /**
     *
     * @return
     */
    public String getGlosa() {
        return glosa;
    }

    /**
     *
     * @param glosa
     */
    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Region{" + "codRegion=" + codRegion + ", Glosa=" 
                + glosa + '}';
    }
}
