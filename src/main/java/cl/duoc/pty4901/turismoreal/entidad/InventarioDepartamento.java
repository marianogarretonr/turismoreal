/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class InventarioDepartamento {
    private String estadoLuces;
    private String microondas;
    private String hervidor;
    private String juguera;
    private String wifi;
    private String tvCable;
    private int cantFrazadas;
    private int cantVasos;
    private int cantTazas;
    private int cantPlatos;
    private int cantCubiertos;
    private int IdDepto;
    private int idInventario;
    
    public InventarioDepartamento() {
    }

    public InventarioDepartamento(String estadoLuces, String microondas, String hervidor, String juguera, 
            String wifi, String tvCable, int cantFrazadas, int cantVasos, int cantTazas, int cantPlatos, int cantCubiertos, 
            int IdDepto,int idInventario) {
        this.estadoLuces = estadoLuces;
        this.microondas = microondas;
        this.hervidor = hervidor;
        this.juguera = juguera;
        this.wifi = wifi;
        this.tvCable = tvCable;
        this.cantFrazadas = cantFrazadas;
        this.cantVasos = cantVasos;
        this.cantTazas = cantTazas;
        this.cantPlatos = cantPlatos;
        this.cantCubiertos = cantCubiertos;
        this.IdDepto = IdDepto;
        this.idInventario = idInventario;
    }
    
    public InventarioDepartamento(String estadoLuces, String microondas, String hervidor, String juguera, String wifi, 
            String tvCable, int cantFrazadas, int cantVasos, int cantTazas, int cantPlatos, int cantCubiertos, int IdDepto) {
        this.estadoLuces = estadoLuces;
        this.microondas = microondas;
        this.hervidor = hervidor;
        this.juguera = juguera;
        this.wifi = wifi;
        this.tvCable = tvCable;
        this.cantFrazadas = cantFrazadas;
        this.cantVasos = cantVasos;
        this.cantTazas = cantTazas;
        this.cantPlatos = cantPlatos;
        this.cantCubiertos = cantCubiertos;
        this.IdDepto = IdDepto;
    }
    
    public String getEstadoLuces() {
        return estadoLuces;
    }

    public void setEstadoLuces(String estadoLuces) {
        this.estadoLuces = estadoLuces;
    }

    public String getMicroondas() {
        return microondas;
    }

    public void setMicroondas(String microondas) {
        this.microondas = microondas;
    }

    public String getHervidor() {
        return hervidor;
    }

    public void setHervidor(String hervidor) {
        this.hervidor = hervidor;
    }

    public String getJuguera() {
        return juguera;
    }

    public void setJuguera(String juguera) {
        this.juguera = juguera;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getTvCable() {
        return tvCable;
    }

    /**
     * @param idInventario
     * @param estadoLuces
     * @param microondas
     * @param hervidor
     * @param juguera
     * @param wifi
     * @param tvCable
     * @param cantFrazadas
     * @param cantVasos
     * @param cantTazas
     * @param cantPlatos
     * @param cantCubiertos
     * @param Departamento
     */
    public void setTvCable(String tvCable) {
        this.tvCable = tvCable;
    }

    /**
     * @return the cantFrazadas
     */
    public int getCantFrazadas() {
        return cantFrazadas;
    }

    /**
     * @param cantFrazadas the cantFrazadas to set
     */
    public void setCantFrazadas(int cantFrazadas) {
        this.cantFrazadas = cantFrazadas;
    }

    /**
     * @return the cantVasos
     */
    public int getCantVasos() {
        return cantVasos;
    }

    /**
     * @param cantVasos the cantVasos to set
     */
    public void setCantVasos(int cantVasos) {
        this.cantVasos = cantVasos;
    }

    /**
     * @return the cantTazas
     */
    public int getCantTazas() {
        return cantTazas;
    }

    /**
     * @param cantTazas the cantTazas to set
     */
    public void setCantTazas(int cantTazas) {
        this.cantTazas = cantTazas;
    }

    /**
     * @return the cantPlatos
     */
    public int getCantPlatos() {
        return cantPlatos;
    }

    /**
     * @param cantPlatos the cantPlatos to set
     */
    public void setCantPlatos(int cantPlatos) {
        this.cantPlatos = cantPlatos;
    }

    /**
     * @return the cantCubiertos
     */
    public int getCantCubiertos() {
        return cantCubiertos;
    }

    /**
     * @param cantCubiertos the cantCubiertos to set
     */
    public void setCantCubiertos(int cantCubiertos) {
        this.cantCubiertos = cantCubiertos;
    }

    public int getIdDepto() {
        return IdDepto;
    }

    public void setIdDepto(int IdDepto) {
        this.IdDepto = IdDepto;
    }
    
      public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    @Override
    public String toString() {
        return "InventarioDepartamento{" + "estadoLuces=" + estadoLuces + ", microondas=" + microondas 
                + ", hervidor=" + hervidor + ", juguera=" + juguera + ", wifi=" + wifi + ", tvCable=" 
                + tvCable + ", cantFrazadas=" + cantFrazadas + ", cantVasos=" + cantVasos + ", cantTazas=" 
                + cantTazas + ", cantPlatos=" + cantPlatos + ", cantCubiertos=" + cantCubiertos + ", IdDepto=" 
                + IdDepto + ", idInventario=" + idInventario + '}';
    }




  
    
}
