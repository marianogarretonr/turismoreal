/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

import java.util.Date;

/**
 *
 * @author maria
 */
public class Reserva {
    private int codReserva;
    //private Departamento objDepartamento;
    private Usuario objUsuario;
    private Date fechaInicio;
    private Date fechaFin;
    private int total_reserva;
    private char pago_reserva;
    private int monto_reserva;
    private int monto_pendiente;
    private int total_multa;
    /**private EstadoReserva objReserva;**/
    private int observacion;
    private Departamento ObjDepartamento;


    public Reserva() {
        this.objUsuario = new Usuario();
        this.ObjDepartamento = new Departamento();
    }

    
    public Reserva(int codReserva, Usuario objUsuario, Date fechaInicio, Date fechaFin, int total_reserva, char pago_reserva, int monto_reserva, int monto_pendiente, int total_multa, /**EstadoReserva objReserva,**/ int observacion) {
        this.codReserva = codReserva;
        this.objUsuario = objUsuario;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.total_reserva = total_reserva;
        this.pago_reserva = pago_reserva;
        this.monto_reserva = monto_reserva;
        this.monto_pendiente = monto_pendiente;
        this.total_multa = total_multa;
        /**this.objReserva = objReserva;**/
        this.observacion = observacion;
    }
    
    public Reserva(int codReserva){
        this.codReserva = codReserva;
    }

    public int getCod_reserva() {
        return codReserva;
    }

    public void setCodReserva(int codReserva) {
        this.codReserva = codReserva;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fecha_inicio) {
        this.fechaInicio = fecha_inicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getTotal_reserva() {
        return total_reserva;
    }

    public void setTotal_reserva(int total_reserva) {
        this.total_reserva = total_reserva;
    }

    public char getPago_reserva() {
        return pago_reserva;
    }

    public void setPago_reserva(char pago_reserva) {
        this.pago_reserva = pago_reserva;
    }

    public int getMonto_reserva() {
        return monto_reserva;
    }

    public void setMonto_reserva(int monto_reserva) {
        this.monto_reserva = monto_reserva;
    }

    public int getMonto_pendiente() {
        return monto_pendiente;
    }

    public void setMonto_pendiente(int monto_pendiente) {
        this.monto_pendiente = monto_pendiente;
    }

    public int getTotal_multa() {
        return total_multa;
    }

    public void setTotal_multa(int total_multa) {
        this.total_multa = total_multa;
    }

    /**public EstadoReserva getObjReserva() {
        return objReserva;
    }

    public void setObjReserva(EstadoReserva objReserva) {
        this.objReserva = objReserva;
    }**/

    public int getObservacion() {
        return observacion;
    }

    public void setObservacion(int observacion) {
        this.observacion = observacion;
    }
    
    public Departamento getObjDepartamento() {
        return ObjDepartamento;
    }

    public void setObjDepartamento(Departamento ObjDepartamento) {
        this.ObjDepartamento = ObjDepartamento;
    }

    @Override
    public String toString() {
        return "Reserva{" + "codReserva=" + codReserva + '}';
    }


    
    
}
