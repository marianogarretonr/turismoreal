/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class Departamento {
  private String nombreDepartamento;
    private Ciudad objCiudad;
    private String calle;
    private int nroCalle;
    private String piso;
    private String nroDepartamento;
    private int cantHabitaciones;
    private int cantBanios;
    private int cantPersonas;
    private String aceptaMascotas;
    private String descripcion;
    private EstadoDepartamento objEstadoDepartamento;
    private int id_dpto;
    private int valorDia;
    
    public Departamento() {
    }
    
    /**
     * 
     * @param nombreDepartamento
     * @param calle
     * @param nroCalle
     * @param piso
     * @param nroDepartamento
     * @param cantHabitaciones
     * @param cantBanios
     * @param cantPersonas
     * @param aceptaMascotas
     * @param descripcion
     * @param Ciudad
     * @param EstadoDepartamento
     * @param id_dpto
     * @param valorDia
     */
    public Departamento(String nombreDepartamento, Ciudad objCiudad, String calle, int nroCalle, String piso, 
            String nroDepartamento, int cantHabitaciones, int cantBanios, int cantPersonas, String aceptaMascotas, 
            String descripcion, EstadoDepartamento objEstadoDepartamento, int id_dpto, int valorDia) {
        this.nombreDepartamento = nombreDepartamento;
        this.objCiudad = objCiudad;
        this.calle = calle;
        this.nroCalle = nroCalle;
        this.piso = piso;
        this.nroDepartamento = nroDepartamento;
        this.cantHabitaciones = cantHabitaciones;
        this.cantBanios = cantBanios;
        this.cantPersonas = cantPersonas;
        this.aceptaMascotas = aceptaMascotas;
        this.descripcion = descripcion;
        this.objEstadoDepartamento = objEstadoDepartamento;
        this.id_dpto = id_dpto;
        this.valorDia = valorDia;
    }
    
    public Departamento(String nombreDepartamento, Ciudad objCiudad, String calle, int nroCalle, String piso, 
            String nroDepartamento, int cantHabitaciones, int cantBanios, int cantPersonas, String aceptaMascotas, 
            String descripcion, int valorDia , EstadoDepartamento objEstadoDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
        this.objCiudad = objCiudad;
        this.calle = calle;
        this.nroCalle = nroCalle;
        this.piso = piso;
        this.nroDepartamento = nroDepartamento;
        this.cantHabitaciones = cantHabitaciones;
        this.cantBanios = cantBanios;
        this.cantPersonas = cantPersonas;
        this.aceptaMascotas = aceptaMascotas;
        this.descripcion = descripcion;
        this.valorDia = valorDia;
        this.objEstadoDepartamento = objEstadoDepartamento;
    }

    /**
     * @return the nombreDepartamento
     */
    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    /**
     * @param nombreDepartamento the nombreDepartamento to set
     */
    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    /**
     * @return the calle
     */
    public String getCalle() {
        return calle;
    }

    /**
     * @param calle the calle to set
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * @return the nroCalle
     */
    public int getNroCalle() {
        return nroCalle;
    }

    /**
     * @param nroCalle the nroCalle to set
     */
    public void setNroCalle(int nroCalle) {
        this.nroCalle = nroCalle;
    }

    /**
     * @return the piso
     */
    public String getPiso() {
        return piso;
    }

    /**
     * @param piso the piso to set
     */
    public void setPiso(String piso) {
        this.piso = piso;
    }

    /**
     * @return the nroDepartamento
     */
    public String getNroDepartamento() {
        return nroDepartamento;
    }

    /**
     * @param nroDepartamento the nroDepartamento to set
     */
    public void setNroDepartamento(String nroDepartamento) {
        this.nroDepartamento = nroDepartamento;
    }

    /**
     * @return the cantHabitaciones
     */
    public int getCantHabitaciones() {
        return cantHabitaciones;
    }

    /**
     * @param cantHabitaciones the cantHabitaciones to set
     */
    public void setCantHabitaciones(int cantHabitaciones) {
        this.cantHabitaciones = cantHabitaciones;
    }

    /**
     * @return the cantBanios
     */
    public int getCantBanios() {
        return cantBanios;
    }

    /**
     * @param cantBanios the cantBanios to set
     */
    public void setCantBanios(int cantBanios) {
        this.cantBanios = cantBanios;
    }

    /**
     * @return the cantPersonas
     */
    public int getCantPersonas() {
        return cantPersonas;
    }

    /**
     * @param cantPersonas the cantPersonas to set
     */
    public void setCantPersonas(int cantPersonas) {
        this.cantPersonas = cantPersonas;
    }

    /**
     * @return the aceptaMascotas
     */
    public String isAceptaMascotas() {
        return aceptaMascotas;
    }

    /**
     * @param aceptaMascotas the aceptaMascotas to set
     */
    public void setAceptaMascotas(String aceptaMascotas) {
        this.aceptaMascotas = aceptaMascotas;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the objCiudad
     */
    public Ciudad getObjCiudad() {
        return objCiudad;
    }

    /**
     * @param objCiudad the objCiudad to set
     */
    public void setObjCiudad(Ciudad objCiudad) {
        this.objCiudad = objCiudad;
    }

    /**
     * @return the objEstadoDepartamento
     */
    public EstadoDepartamento getObjEstadoDepartamento() {
        return objEstadoDepartamento;
    }

    /**
     * @param objEstadoDepartamento the objEstadoDepartamento to set
     */
    public void setObjEstadoDepartamento(EstadoDepartamento objEstadoDepartamento) {
        this.objEstadoDepartamento = objEstadoDepartamento;
    }
    
     public int getId_dpto() {
        return id_dpto;
    }

    /**
     * @return the idDepartamento
     */
    public void setId_dpto(int id_dpto) {   
        this.id_dpto = id_dpto;
    }
    
    public void setValorDia(int valorDia){
        this.valorDia = valorDia;
    }
    
    public int getValorDia(){
        return valorDia;
    }

    @Override
    public String toString() {
        return "Departamento{" + "nombreDepartamento=" + nombreDepartamento + ", objCiudad=" + objCiudad + 
                ", calle=" + calle + ", nroCalle=" + nroCalle + ", piso=" + piso + ", nroDepartamento=" 
                + nroDepartamento + ", cantHabitaciones=" + cantHabitaciones + ", cantBanios=" + cantBanios 
                + ", cantPersonas=" + cantPersonas + ", aceptaMascotas=" + aceptaMascotas + ", descripcion=" 
                + descripcion + ", valorDia=" + valorDia + ", objEstadoDepartamento=" + objEstadoDepartamento + ", id_dpto=" + id_dpto + '}';
    }

    
        
}
