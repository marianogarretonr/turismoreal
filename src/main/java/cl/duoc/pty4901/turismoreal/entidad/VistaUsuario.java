/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class VistaUsuario {
  
    private int idVistaUsuario;
    private Vista objVista;
    private Usuario objUsuario;

    /**
     *
     */
    public VistaUsuario() {
    }

    /**
     *
     * @param idVistaUsuario
     * @param objVista
     * @param objUsuario
     */
    public VistaUsuario(int idVistaUsuario, Vista objVista, 
            Usuario objUsuario) {
        this.idVistaUsuario = idVistaUsuario;
        this.objVista = objVista;
        this.objUsuario = objUsuario;
    }

    /**
     *
     * @return
     */
    public int getIdVistaUsuario() {
        return idVistaUsuario;
    }

    /**
     *
     * @param idVistaUsuario
     */
    public void setIdVistaUsuario(int idVistaUsuario) {
        this.idVistaUsuario = idVistaUsuario;
    }

    /**
     *
     * @return
     */
    public Vista getObjVista() {
        return objVista;
    }

    /**
     *
     * @param objVista
     */
    public void setObjVista(Vista objVista) {
        this.objVista = objVista;
    }

    /**
     *
     * @return
     */
    public Usuario getObjUsuario() {
        return objUsuario;
    }

    /**
     *
     * @param objUsuario
     */
    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "VistaUsuario{" + "idVistaUsuario=" + idVistaUsuario 
                + ", objVista=" + objVista + ", objUsuario=" + objUsuario + '}';
    }   
}
