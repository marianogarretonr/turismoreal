/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class Usuario {
    private int rutUsuario;
    private String dvUsuario;
    private String nombreUsuario;
    private String apPaterno;
    private String apMaterno;
    private String nroTelefono;
    private String email;
    private Ciudad objCiudad;
    private String frecuente;
    private EstadoUsuario objEstadoUsuario;
    private TipoUsuario objTipoUsuario;
    private String contrasenia;
    private int id;
    private Usuario objUsuario;
    
    public Usuario() {
    }

    /**
     *
     * @param rutUsuario
     * @param dvUsuario
     * @param ap_paterno
     * @param ap_materno
     * @param nroTelefono
     * @param email
     * @param Ciudad
     * @param frecuente
     * @param EstadoUsuario
     * @param TipoUsuario
     */
    public Usuario(int rutUsuario, String dvUuario, String nombreUsuario, String apPaterno, String apMaterno, String nroTelefono,
            String email, Ciudad objCiudad, String frecuente, EstadoUsuario objEstadoUsuario, TipoUsuario objTipoUsuario) {
        this.rutUsuario = rutUsuario;
        this.dvUsuario = dvUuario;
        this.nombreUsuario = nombreUsuario;
        this.apPaterno = apPaterno;
        this.apMaterno = apMaterno;
        this.nroTelefono = nroTelefono;
        this.email = email;
        this.objCiudad = objCiudad;
        this.frecuente = frecuente;
        this.objEstadoUsuario = objEstadoUsuario;
        this.objTipoUsuario = objTipoUsuario;
    }
    
    /**
     *
     * @param usuario
     * @param contrasenia
     */
    public Usuario(String email, String contrasenia) {
            this.email = email;
           this.contrasenia = contrasenia;
    }

    public Usuario(String email, EstadoUsuario objEstadoUsuario) {
         this.email = email;
        this.objEstadoUsuario = objEstadoUsuario;
    }
 
    public Usuario(int rutUsuario, String apPaterno, String apMaterno, String nombres,String telefono, String email,
            EstadoUsuario objEstadoUsuario, Ciudad objCiudad, TipoUsuario objTipoUsuario){
        this.rutUsuario = rutUsuario;
        this.apMaterno = apMaterno;
        this.apPaterno = apPaterno;
        this.nombreUsuario = nombres;
        this.nroTelefono = telefono;
        //this.dvUsuario = dvRut;
        this.email = email;
        this.objCiudad = objCiudad;
        this.objEstadoUsuario = objEstadoUsuario;
        this.objTipoUsuario = objTipoUsuario;   
    }
    
      
    public int getRutUsuario() {
        return rutUsuario;
    }

    /**
     *
     * @param rutUsuario
     */
    public void setRutUsuario(int rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

    /**
     * @return the dvUsuario
     */
    public String getDvUsuario() {
        return dvUsuario;
    }

    /**
     * @param dvUsuario the dvUsuario to set
     */
    public void setDvUsuario(String dvUsuario) {
        this.dvUsuario = dvUsuario;
    }

    /**
     * @return the nombreUsuario
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    /**
     * @param nombreUsuario the nombreUsuario to set
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    /**
     * @return the apPaterno
     */
    public String getApPaterno() {
        return apPaterno;
    }

    /**
     * @param apPaterno the apPaterno to set
     */
    public void setApPaterno(String apPaterno) {
        this.apPaterno = apPaterno;
    }

    /**
     * @return the apMaterno
     */
    public String getApMaterno() {
        return apMaterno;
    }

    /**
     * @param apMaterno the apMaterno to set
     */
    public void setApMaterno(String apMaterno) {
        this.apMaterno = apMaterno;
    }

    /**
     * @return the nroTelefono
     */
    public String getNroTelefono() {
        return nroTelefono;
    }

    /**
     * @param nroTelefono the nroTelefono to set
     */
    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the objCiudad
     */
    public Ciudad getObjCiudad() {
        return objCiudad;
    }

    /**
     * @param objCiudad the objCiudad to set
     */
    public void setObjCiudad(Ciudad objCiudad) {
        this.objCiudad = objCiudad;
    }

    /**
     * @return the frecuente
     */
    public String getFrecuente() {
        return frecuente;
    }

    /**
     * @param frecuente the frecuente to set
     */
    public void setFrecuente(String frecuente) {
        this.frecuente = frecuente;
    }
    
    /**
     * @return the objEstadoUsuario
     */
    public EstadoUsuario getObjEstadoUsuario() {
        return objEstadoUsuario;
    }

    /**
     * @param objEstadoUsuario the objEstadoUsuario to set
     */
    public void setObjEstadoUsuario(EstadoUsuario objEstadoUsuario) {
        this.objEstadoUsuario = objEstadoUsuario;
    }

    /**
     * @return the objTipoUsuario
     */
    public TipoUsuario getObjTipoUsuario() {
        return objTipoUsuario;
    }

    /**
     * @param objTipoUsuario the objTipoUsuario to set
     */
    public void setObjTipoUsuario(TipoUsuario objTipoUsuario) {
        this.objTipoUsuario = objTipoUsuario;
    }

    /**
     * @return the contrasenia
     */
    public String getContrasenia() {
        return contrasenia;
    }

    /**
     * @param contrasenia the contrasenia to set
     */
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public int getId() {
        return id;
    }

    /**
     * @param contrasenia the contrasenia to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public Usuario getObjUsuario() {
    return objUsuario;
    }

    /**
     *
     * @param objProveedor
     */
    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }
    
     /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Usuario{" + "rutUsuaro=" + rutUsuario + ", dvUsuario=" + dvUsuario 
                + ", nombreUsuario=" + nombreUsuario + ", ap_paterno=" + apPaterno + ",ap_materno=" + apMaterno
                + ", nro_telefono=" +nroTelefono + ",email=" + email + ", objCiudad=" + objCiudad 
                + ", frecuente="+ frecuente +", objEstadoUsuario=" + objEstadoUsuario + ", objTipoUsuario=" + objTipoUsuario
                +'}';
    }
    
}