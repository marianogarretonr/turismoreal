/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class CheckOUT {
    private String multa;
    private String observacion;
    private String fechaCheckOut;
    private int codReserva;
    private int idCheckOut;

    public CheckOUT() {
    }

    public CheckOUT(String multa, String observacion, String fechaCheckOut, int codReserva, int idCheckOut) {
        this.multa = multa;
        this.observacion = observacion;
        this.fechaCheckOut = fechaCheckOut;
        this.codReserva = codReserva;
        this.idCheckOut = idCheckOut;
    }
    
        public CheckOUT(String multa, String observacion, String fechaCheckOut, int codReserva) {
        this.multa = multa;
        this.observacion = observacion;
        this.fechaCheckOut = fechaCheckOut;
        this.codReserva = codReserva;
    }
    
    public String getMulta() {
        return multa;
    }

    public void setMulta(String entregaRegalo) {
        this.multa = entregaRegalo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFechaCheckOut() {
        return fechaCheckOut;
    }

    public void setFechaCheckOut(String fechaCheckOut) {
        this.fechaCheckOut = fechaCheckOut;
    }

    public int getCodReserva() {
        return codReserva;
    }

    public void setCodReserva(int codReserva) {
        this.codReserva = codReserva;
    }

    public int getIdCheckOut() {
        return idCheckOut;
    }

    public void setIdCheckOut(int idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    @Override
    public String toString() {
        return "CheckOut{" + "multa=" + multa + ",  observacion=" + observacion + ", fechaCheckOut=" + fechaCheckOut + ", codReserva=" + codReserva + ", idCheckOut=" + idCheckOut + '}';
    }

}

