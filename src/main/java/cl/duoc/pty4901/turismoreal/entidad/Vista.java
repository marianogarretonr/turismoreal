/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class Vista {
private int idVista;
    private String nombreVista;

    /**
     *
     */
    public Vista() {
    }

    /**
     *
     * @param idVista
     * @param nombreVista
     */
    public Vista(int idVista, String nombreVista) {
        this.idVista = idVista;
        this.nombreVista = nombreVista;
    }

    /**
     *
     * @return
     */
    public int getIdVista() {
        return idVista;
    }

    /**
     *
     * @param idVista
     */
    public void setIdVista(int idVista) {
        this.idVista = idVista;
    }

    /**
     *
     * @return
     */
    public String getNombreVista() {
        return nombreVista;
    }

    /**
     *
     * @param nombreVista
     */
    public void setNombreVista(String nombreVista) {
        this.nombreVista = nombreVista;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Vista{" + "idVista=" + idVista + ", nombreVista=" 
                + nombreVista + '}';
    } 
}
