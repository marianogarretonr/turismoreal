/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class Ciudad {
    private int codCiudad;
    private String nombreCiudad;
    private Region objRegion;

    /**
     *
     */
    public Ciudad() {
    }

    /**
     *
     * @param codCiudad
     * @param nombreCiudad
     * @param objRegion
     */
    public Ciudad(int codCiudad, String nombreCiudad, Region objRegion) {
        this.codCiudad = codCiudad;
        this.nombreCiudad = nombreCiudad;
        this.objRegion = objRegion;
    }
    
    /**
     *
     * @param codCiudad
     * @param nombreCiudad
     */
    public Ciudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
       
    }

    public Ciudad(int codCiudad) {
        this.codCiudad = codCiudad;
    }
    
    
    

    /**
     *
     * @return
     */
    public int getCodCiudad() {
        return codCiudad;
    }

    /**
     *
     * @param codCiudad
     */
    public void setCodCiudad(int codCiudad) {
        this.codCiudad = codCiudad;
    }

    /**
     *
     * @return
     */
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    /**
     *
     * @param nombreCiudad
     */
    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    /**
     *
     * @return
     */
    public Region getObjRegion() {
        return objRegion;
    }

    /**
     *
     * @param objRegion
     */
    public void setObjRegion(Region objRegion) {
        this.objRegion = objRegion;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return nombreCiudad;
    }

}

