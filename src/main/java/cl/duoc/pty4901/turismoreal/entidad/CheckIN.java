/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class CheckIN {
    private String entregaRegalo;
    private String estadoPago;
    private String observacion;
    private String fechaCheckIn;
    private int codReserva;
    private int idCheckIn;

    public CheckIN() {
    }

    public CheckIN(String entregaRegalo, String estadoPago, String observacion, String fechaCheckIn, int codReserva, int idCheckIn) {
        this.entregaRegalo = entregaRegalo;
        this.estadoPago = estadoPago;
        this.observacion = observacion;
        this.fechaCheckIn = fechaCheckIn;
        this.codReserva = codReserva;
        this.idCheckIn = idCheckIn;
    }
    
        public CheckIN(String entregaRegalo, String estadoPago, String observacion, String fechaCheckIn, int codReserva) {
        this.entregaRegalo = entregaRegalo;
        this.estadoPago = estadoPago;
        this.observacion = observacion;
        this.fechaCheckIn = fechaCheckIn;
        this.codReserva = codReserva;
    }
    
    public String getEntregaRegalo() {
        return entregaRegalo;
    }

    public void setEntregaRegalo(String entregaRegalo) {
        this.entregaRegalo = entregaRegalo;
    }

    public String getEstadoPago() {
        return estadoPago;
    }

    public void setEstadoPago(String estadoPago) {
        this.estadoPago = estadoPago;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFechaCheckIn() {
        return fechaCheckIn;
    }

    public void setFechaCheckIn(String fechaCheckIn) {
        this.fechaCheckIn = fechaCheckIn;
    }

    public int getCodReserva() {
        return codReserva;
    }

    public void setCodReserva(int codReserva) {
        this.codReserva = codReserva;
    }

    public int getIdCheckIn() {
        return idCheckIn;
    }

    public void setIdCheckIn(int idCheckIn) {
        this.idCheckIn = idCheckIn;
    }

    @Override
    public String toString() {
        return "CheckIN{" + "entregaRegalo=" + entregaRegalo + ", estadoPago=" + estadoPago + ", observacion=" + observacion + ", fechaCheckIn=" + fechaCheckIn + ", codReserva=" + codReserva + ", idCheckIn=" + idCheckIn + '}';
    }

}

