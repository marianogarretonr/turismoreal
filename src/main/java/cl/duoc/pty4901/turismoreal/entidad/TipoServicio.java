/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author maria
 */
public class TipoServicio {
   private int id_tipo_servicio;
    private String descripcion;

    public TipoServicio() {
    }

    public TipoServicio(int id_tipo_servicio, String descripcion) {
        this.id_tipo_servicio = id_tipo_servicio;
        this.descripcion = descripcion;
    }
    
      public TipoServicio(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId_tipo_servicio() {
        return id_tipo_servicio;
    }

    public void setId_tipo_servicio(int id_tipo_servicio) {
        this.id_tipo_servicio = id_tipo_servicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
    
}
