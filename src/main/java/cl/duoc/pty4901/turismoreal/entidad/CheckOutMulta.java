/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class CheckOutMulta {
    private Multa objMulta;
    private int idCheckOut;
    private int id;

    public CheckOutMulta() {
    }

    public CheckOutMulta(Multa objMulta, int idCheckOut) {
        this.objMulta = objMulta;
        this.idCheckOut = idCheckOut;
    }

    public Multa getObjMulta() {
        return objMulta;
    }

    public void setObjMulta(Multa objMulta) {
        this.objMulta = objMulta;
    }

    public int getIdCheckOut() {
        return idCheckOut;
    }

    public void setIdCheckOut(int idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return "CheckOutMulta{" + "objMulta=" + objMulta + ", idCheckOut=" + idCheckOut +'}';
    }
    
}