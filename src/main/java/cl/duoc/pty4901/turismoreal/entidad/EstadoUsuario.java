/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class EstadoUsuario {
    private int idEstadoUsuario;
    private String descripcion;
    
    public EstadoUsuario(){
    }
    
      /**
     *
     * @param idEstadoUsuario
     * @param descripcion
     */
    public EstadoUsuario(int idEstadoUsuario, String descripcion) {
        this.idEstadoUsuario = idEstadoUsuario;
        this.descripcion = descripcion;
    }
    
    public EstadoUsuario(String descripcion){
        this.descripcion = descripcion;  
    }
    
    public EstadoUsuario(int idEstadoUsuario){
        this.idEstadoUsuario = idEstadoUsuario;
    }

    /**
     * @return the idEstadoUsuario
     */
    public int getIdEstadoUsuario() {
        return idEstadoUsuario;
    }

    /**
     * @param idEstadoUsuario the idEstadoUsuario to set
     */
    public void setIdEstadoUsuario(int idEstadoUsuario) {
        this.idEstadoUsuario = idEstadoUsuario;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Override
    public String toString() {
        return descripcion;
    }

    
}
