/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class Multa {
    private int valor;
    private String descripcion;
    private int idMulta;

    /**
     *
     */
    public Multa() {
    }

    /**
     *
     * @param codCiudad
     * @param nombreCiudad
     * @param objRegion
     */
    public Multa(int valor, String descripcion, int idMulta) {
        this.valor = valor;
        this.descripcion = descripcion;
        this.idMulta = idMulta;
    }

    public Multa(String descripcion){
        this.descripcion = descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdMulta() {
        return idMulta;
    }

    public void setIdMulta(int idMulta) {
        this.idMulta = idMulta;
    }
       
    
    @Override
    public String toString() {
        return descripcion;
    }

}

