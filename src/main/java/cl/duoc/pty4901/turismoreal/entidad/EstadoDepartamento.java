/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.pty4901.turismoreal.entidad;

/**
 *
 * @author santa
 */
public class EstadoDepartamento {
    private int idEstado;
    private String glosa;
    
    public EstadoDepartamento() {
    }
    
    /**
     * @param idEstado
     * @param glosa
     */
    
    public EstadoDepartamento (int idEstado, String glosa) {
        this.idEstado = idEstado;
        this.glosa = glosa;        
    }
    
     public EstadoDepartamento (String glosa) {
        this.glosa = glosa;        
    }

    /**
     * @return the idEstado
     */
    public int getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    /**
     * @return the glosa
     */
    public String getGlosa() {
        return glosa;
    }

    /**
     * @param glosa the glosa to set
     */
    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return glosa;
    }    
}
